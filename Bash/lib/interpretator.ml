open Ast2
open Outside

(*...........interpritator.................*)
(*////////////////////////////*)
(*////////////////////////////*)
(*////////////////////////////*)
(*////////////////////////////*)

(*нужно*)
let check_str s =
  (*let _ = print_string ("\ntest check:" ^ s ^ "-\n") in*)
  match int_of_string s with exception _ -> false | _ -> true

(*
  try int_of_string s |> ignore; true
  with Failure _ -> false
*)
let hash_table_list h = Hashtbl.fold (fun k v acc -> (k, v) :: acc) h []

let merdge_tables table_source table_receiver =
  let lst = hash_table_list table_source in
  List.map
    (fun bag ->
      match bag with str, var -> Hashtbl.replace table_receiver str var)
    lst
  |> ignore

let merdge_tables_list list_source table_receiver =
  List.map
    (fun bag ->
      match bag with str, var -> Hashtbl.replace table_receiver str var)
    list_source
  |> ignore

let list_to_pair lst =
  let rec helper i list =
    match list with
    | head :: tail -> (string_of_int i, head) :: helper (i + 1) tail
    | [] -> [] in
  helper 1 lst

(*-----------------------------------------------------------------------------------*)
module type MONAD = sig
  type 'a t

  val return : 'a -> 'a t
  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  val ( >> ) : 'a t -> 'b t -> 'b t
end

module type MONADERROR = sig
  include MONAD

  val error : string -> 'a t
end

module Value_out (M : MONADERROR) = struct
  open M

  (*будет кидать исключение Not_found*)
  let type_out_elem elem =
    let heap = Hashtbl.create 1 in
    let converter e_t =
      e_t
      >>= fun el ->
      let _ = Hashtbl.add heap 1 el in
      error "" in
    let _ = converter elem in
    Hashtbl.find heap 1

  (*будет кидать исключение Not_found*)
  let type_out_lst lst =
    let heap = Hashtbl.create 123 in
    let rec list_converter list i =
      match list with
      | head :: tail ->
          head
          >>= fun h ->
          let _ = Hashtbl.add heap i h in
          let _ = list_converter tail (i + 1) in
          return h
      | [] -> error "" in
    let index = List.length lst in
    let rec roller i =
      if i < index then
        let temp_lst = roller (i + 1) in
        let elem = Hashtbl.find heap i in
        elem :: temp_lst
      else [Hashtbl.find heap i] in
    let _ = list_converter lst 1 in
    roller 1
end

module Result = struct
  type 'a t = ('a, string) Result.t

  let ( >>= ) = Result.bind
  let return = Result.ok
  let error = Result.error
  let ( >> ) x f = x >>= fun _ -> f
end

(*---------------------------------*)

let string_to_var_contain_lst list =
  let val_lst =
    List.map
      (fun elem ->
        if check_str elem then Value (VInt, elem) else Value (VString, elem))
      list in
  let cont_lst =
    List.map
      (fun elem -> {sig_value= elem; is_array= false; array_value= []})
      val_lst in
  cont_lst

(*----------------------------------*)

(*-----------------Интерпретируем арифметические выражения-----------------------------------------------------------*)
module Interpreter_Arifm (M : MONADERROR) = struct
  open M

  let eval get_value input_arifm =
    let rec helper_eval input_arifm =
      match input_arifm with
      (* means a + b *)
      | Plus (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right >>= fun right_val -> return (left_val + right_val)
      (* means a - b *)
      | Minus (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right >>= fun right_val -> return (left_val - right_val)
      (* means a * b *)
      | Multi (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right >>= fun right_val -> return (left_val * right_val)
      (* means a / b *)
      | Divide (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right >>= fun right_val -> return (left_val / right_val)
      (* "x", "y", "n", etc. *)
      | Container (Int x) -> return x
      | Container (String _) -> return 0
      | Container (Variable var) -> (
        match get_value var with Error str -> error str | Ok x -> return x )
      | Container (Float _) -> return 0
      (*a && b *)
      | AndAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val * right_val in
          if rezult = 0 then return 0 else return 1
      (*a || b *)
      | OrAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = (left_val * left_val) + (right_val * right_val) in
          if rezult = 0 then return 0 else return 1
      (*a > b *)
      | GreatAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val > right_val in
          if rezult then return 1 else return 0
      (*a < b *)
      | LessAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val < right_val in
          if rezult then return 1 else return 0
      (*a >= b *)
      | EcGreatAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val >= right_val in
          if rezult then return 1 else return 0
      (*a <= b *)
      | EcLessAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val <= right_val in
          if rezult then return 1 else return 0
      (*a == b *)
      | EcualyAr (left, right) ->
          helper_eval left
          >>= fun left_val ->
          helper_eval right
          >>= fun right_val ->
          let rezult = left_val = right_val in
          if rezult then return 1 else return 0 in
    helper_eval input_arifm
end

let rec get_value_of_vari_int global_cont local_cont name =
  let module E = Interpreter_Arifm (Result) in
  match name with
  (*Variable; SimpleVari => $s*)
  | SimpleVari var ->
      let localiti = Hashtbl.mem local_cont var in
      if localiti then
        match (Hashtbl.find local_cont var).sig_value with
        | Value (VString, s) ->
            if check_str s then Ok (int_of_string s) else Ok 0
        | Value (VInt, str_int) -> Ok (int_of_string str_int)
      else
        let globaliti = Hashtbl.mem global_cont var in
        if globaliti then
          match (Hashtbl.find global_cont var).sig_value with
          | Value (VString, s) ->
              if check_str s then Ok (int_of_string s) else Ok 0
          | Value (VInt, str_int) -> Ok (int_of_string str_int)
        else Ok 0
  (*обращение к массиву; ArrayVari => array[1+1] или array[$s]*)
  | ArrayVari (label, index_arifm) -> (
    match E.eval (get_value_of_vari_int global_cont local_cont) index_arifm with
    | Ok index -> (
      try
        let localiti = Hashtbl.mem local_cont label in
        if localiti then
          let lst = (Hashtbl.find local_cont label).array_value in
          let target =
            match List.nth lst index with
            | x -> x
            | exception _ -> failwith "List error" in
          match target with
          | Value (VString, s) ->
              if check_str s then Ok (int_of_string s) else Ok 0
          | Value (VInt, str_int) -> Ok (int_of_string str_int)
        else
          let globaliti = Hashtbl.mem global_cont label in
          if globaliti then
            let lst = (Hashtbl.find global_cont label).array_value in
            let target =
              match List.nth lst index with
              | x -> x
              | exception _ -> failwith "List error" in
            match target with
            | Value (VString, s) ->
                if check_str s then Ok (int_of_string s) else Ok 0
            | Value (VInt, str_int) -> Ok (int_of_string str_int)
          else Ok 0
      with Failure str -> Error str )
    | Error str -> Error ("in array" ^ str) )
  (*${ss}, но не ${$s}*)
  | Braces variable -> get_value_of_vari_int global_cont local_cont variable
  | _ -> Error "can not get the value"

(*/////////////////////////////////////////////////////////////////////*)
(*---------------------------------------------------------------------------------------------*)
(*может дать исключение Failure*)
let rec get_value_of_container_string global_cont local_cont arg =
  let module E = Interpreter_Arifm (Result) in
  match arg with
  | Int x -> Ok (string_of_int x)
  | Float _ -> Error "flaot"
  | String str -> Ok str
  (*Variable; SimpleVari => $s*)
  | Variable (SimpleVari var) ->
      let localiti = Hashtbl.mem local_cont var in
      if localiti then
        match (Hashtbl.find local_cont var).sig_value with
        | Value (VString, str) -> Ok str
        | Value (VInt, str_int) -> Ok str_int
      else
        let globaliti = Hashtbl.mem global_cont var in
        if globaliti then
          match (Hashtbl.find global_cont var).sig_value with
          | Value (VString, str) -> Ok str
          | Value (VInt, str_int) -> Ok str_int
        else Ok ""
  (*обращение к массиву; ArrayVari => array[1+1] или array[$s]*)
  | Variable (ArrayVari (label, index_arifm)) -> (
    match E.eval (get_value_of_vari_int global_cont local_cont) index_arifm with
    | Ok index -> (
      try
        let localiti = Hashtbl.mem local_cont label in
        if localiti then
          let lst = (Hashtbl.find local_cont label).array_value in
          let target =
            match List.nth lst index with
            | x -> x
            | exception _ -> failwith "List error" in
          match target with
          | Value (VString, str) -> Ok str
          | Value (VInt, str_int) -> Ok str_int
        else
          let globaliti = Hashtbl.mem global_cont label in
          if globaliti then
            let lst = (Hashtbl.find global_cont label).array_value in
            let target =
              match List.nth lst index with
              | x -> x
              | exception _ -> failwith "List error" in
            match target with
            | Value (VString, str) -> Ok str
            | Value (VInt, str_int) -> Ok str_int
          else Ok ""
      with Failure _ | Not_found -> Error "not found value in array" )
    | Error str -> Error ("in array" ^ str) )
  (*${ss}, но не ${$s}*)
  | Variable (Braces variable) ->
      get_value_of_container_string global_cont local_cont (Variable variable)
  | _ -> Error "can not get the value of sting"

let rec get_value_of_container_int global_cont local_cont arg =
  let module E = Interpreter_Arifm (Result) in
  match arg with
  | Int x -> Ok x
  | Float _ -> Ok 0
  | String _ -> Ok 0
  (*Variable; SimpleVari => $s*)
  | Variable (SimpleVari var) ->
      let localiti = Hashtbl.mem local_cont var in
      if localiti then
        match (Hashtbl.find local_cont var).sig_value with
        | Value (VString, s) ->
            if check_str s then Ok (int_of_string s) else Ok 0
        | Value (VInt, str_int) -> Ok (int_of_string str_int)
      else
        let globaliti = Hashtbl.mem global_cont var in
        if globaliti then
          match (Hashtbl.find global_cont var).sig_value with
          | Value (VString, s) ->
              if check_str s then Ok (int_of_string s) else Ok 0
          | Value (VInt, str_int) -> Ok (int_of_string str_int)
        else Error "no int in variable"
  (*обращение к массиву; ArrayVari => array[1+1] или array[$s]*)
  | Variable (ArrayVari (label, index_arifm)) -> (
    match E.eval (get_value_of_vari_int global_cont local_cont) index_arifm with
    | Ok index -> (
      try
        let localiti = Hashtbl.mem local_cont label in
        if localiti then
          let lst = (Hashtbl.find local_cont label).array_value in
          let target =
            match List.nth lst index with
            | x -> x
            | exception _ -> failwith "List error" in
          match target with
          | Value (VString, s) ->
              if check_str s then Ok (int_of_string s) else Ok 0
          | Value (VInt, str_int) -> Ok (int_of_string str_int)
        else
          let globaliti = Hashtbl.mem global_cont label in
          if globaliti then
            let lst = (Hashtbl.find global_cont label).array_value in
            let target =
              match List.nth lst index with
              | x -> x
              | exception _ -> failwith "List error" in
            match target with
            | Value (VString, s) ->
                if check_str s then Ok (int_of_string s) else Ok 0
            | Value (VInt, str_int) -> Ok (int_of_string str_int)
          else Error "no int in array"
      with Failure str -> Error str )
    | Error str -> Error str )
  (*${ss}, но не ${$s}*)
  | Variable (Braces variable) ->
      get_value_of_container_int global_cont local_cont (Variable variable)
  | _ -> Error "can not get int value"

(*----------------------Интерпритируем strOp---------------------------------------------------*)

module Interpreter_StrOp (M : MONADERROR) = struct
  let eval get_value_str get_value_int input_arifm =
    let helper_eval input_arifm =
      match input_arifm with
      (*${#string}*)
      | Leng cont -> (
        try
          let str =
            match get_value_str cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          string_of_int (String.length str)
        with Failure _ -> "0" (*$ {string: position} *) )
      | PicFromPos (str, pos) -> (
        try
          let str_val =
            match get_value_str str with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          let controled_val =
            match get_value_int pos with
            | Error str -> failwith (str ^ "--bad value")
            | Ok x -> x in
          let mystr = str_val in
          let mypos =
            if controled_val < 0 then String.length mystr - controled_val
            else controled_val in
          try String.sub mystr mypos (String.length mystr)
          with Invalid_argument _ -> ""
        with Failure _ -> "" )
      (*${string:position:length}*)
      | PicFromPosLeng (str, pos, len) -> (
        try
          let str_val =
            match get_value_str str with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          let pos_val =
            match get_value_int pos with
            | Error str -> failwith (str ^ "--bad value")
            | Ok x -> x in
          let mylen =
            match get_value_int len with
            | Error str -> failwith (str ^ "--bad value")
            | Ok x -> x in
          let mypos =
            if pos_val < 0 then String.length str_val - pos_val else pos_val
          in
          try String.sub str_val mypos mylen with Invalid_argument _ -> ""
        with Failure _ -> "" )
      (*${string#substring}*)
      | CutBegLess (str_cont, substr_cont) -> (
        try
          let strcont_val =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          let substr_val =
            match get_value_str substr_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          if String.sub strcont_val 0 (String.length substr_val) = substr_val
          then
            String.sub strcont_val (String.length substr_val)
              (String.length strcont_val)
          else strcont_val
        with _ -> "" )
      (*${string%substring}*)
      | CutEndLess (str_cont, substr_cont) -> (
        try
          let strcont_val =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          let substrcont_val =
            match get_value_str substr_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok x -> x in
          if
            String.sub strcont_val
              (String.length strcont_val - String.length substrcont_val)
              (String.length strcont_val)
            = substrcont_val
          then
            String.sub strcont_val 0
              (String.length strcont_val - String.length substrcont_val)
          else strcont_val
        with _ -> "" )
      (*${string##substring}*)
      | CutBegMore (str_cont, substr_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let substr =
            match get_value_str substr_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          if String.sub str 0 (String.length substr) = substr then
            String.sub str (String.length substr) (String.length str)
          else str
        with Failure _ | Not_found -> "" )
      (*${string%%substring}*)
      | CutEndMore (str_cont, substr_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let substr =
            match get_value_str substr_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          if
            String.sub str
              (String.length str - String.length substr)
              (String.length substr)
            = substr
          then String.sub str 0 (String.length str - String.length substr)
          else str
        with Not_found | Failure _ -> "" )
      (*${string/pattern/replacement}*)
      | ReplaceFirst (str_cont, patt_cont, repl_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let pattern =
            match get_value_str patt_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let replacement =
            match get_value_str repl_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          try Str.replace_first (Str.regexp pattern) replacement str
          with Not_found -> str
        with Not_found | Failure _ -> "" )
      (*${string//pattern/replacement}*)
      | ReplaceAll (str_cont, patt_cont, repl_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let pattern =
            match get_value_str patt_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let replacement =
            match get_value_str repl_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          try Str.global_replace (Str.regexp pattern) replacement str
          with Not_found -> str
        with Failure _ | Not_found -> "" )
      (*${string/#pattern/replacement}*)
      | ReplaceBeg (str_cont, patt_cont, repl_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let pattern =
            match get_value_str patt_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let replacement =
            match get_value_str repl_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          try
            if String.sub str 0 (String.length pattern) = pattern then
              try Str.replace_first (Str.regexp pattern) replacement str
              with Not_found -> str
            else str
          with Invalid_argument _ -> str
        with Failure _ | Not_found -> "" )
      (*${string/%pattern/replacement}*)
      | ReplaceEnd (str_cont, patt_cont, repl_cont) -> (
        try
          let str =
            match get_value_str str_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let pattern =
            match get_value_str patt_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          let replacement =
            match get_value_str repl_cont with
            | Error str -> failwith (str ^ "--string bad value")
            | Ok s -> s in
          try
            if
              String.sub str
                (String.length str - String.length pattern)
                (String.length str)
              = pattern
            then
              String.sub str 0 (String.length str - String.length pattern)
              ^ replacement
            else str
          with Invalid_argument _ -> str
        with Failure _ | Not_found -> "" ) in
    helper_eval input_arifm
end

(*----------------------------------Интерпритируем word---------------------------------------------------*)

module Interpreter_Word (M : MONADERROR) = struct
  open M
  open Value_out (M)

  let eval get_value_str input_arifm =
    let rec helper_eval input_arifm =
      match input_arifm with
      | WString cont -> (
        try
          let str =
            match get_value_str cont with
            | Error str -> failwith (str ^ "--string bad value in word")
            | Ok s -> s in
          return [str]
        with Failure str -> error str )
      | WbrExp lst -> (
        try
          let word_lst = match lst with InBracExpan x -> x in
          let list_of_lists = type_out_lst (List.map helper_eval word_lst) in
          return (List.concat list_of_lists)
        with Not_found -> error "Not_found value in ${}" )
      | WStringCont (bag, lst) -> (
        try
          let str =
            match get_value_str bag with
            | Error str -> failwith (str ^ "--string bad value in word")
            | Ok s -> s in
          helper_eval lst
          >>= fun st_list -> return (List.map (fun elem -> str ^ elem) st_list)
        with Failure str -> error str )
      | WbrExpCont (brexp_lst, bag) -> (
        try
          let sec_str_lst = type_out_elem (helper_eval bag) in
          let word_lst = match brexp_lst with InBracExpan x -> x in
          let fir_str_lst =
            List.concat (type_out_lst (List.map helper_eval word_lst)) in
          let conct_lst =
            List.map
              (fun elem ->
                List.map (fun sec_elem -> elem ^ sec_elem) sec_str_lst)
              fir_str_lst in
          return (List.concat conct_lst)
        with Not_found -> error "Not_found value in ${}" ) in
    helper_eval input_arifm
end

(*----------------------Интерпритируем argExp---------------------------------------------------*)

module Interpreter_ArgExpr_bash_all (M : MONADERROR) = struct
  open M
  open Value_out (M)

  let rec eval_arg fun_get_value_str fun_get_value_int fun_get_value_vari_int
      functions_all hash_table_global hash_table_local input_arifm =
    let module Inter_arifm = Interpreter_Arifm (Result) in
    let module Inter_word = Interpreter_Word (Result) in
    let module Inter_strop = Interpreter_StrOp (Result) in
    let get_value_str = fun_get_value_str hash_table_global hash_table_local in
    let get_value_int = fun_get_value_int hash_table_global hash_table_local in
    let get_value_vari_int =
      fun_get_value_vari_int hash_table_global hash_table_local in
    let helper_eval_arg input_arifm =
      match input_arifm with
      (*${#string}*)
      | StringOp str_strop ->
          let rezult = Inter_strop.eval get_value_str get_value_int str_strop in
          return (Some [rezult])
      (*{a,b,c}, или string*)
      | Word wd -> (
          let rezult_t = Inter_word.eval get_value_str wd in
          match rezult_t with
          | Ok st_list -> return (Some st_list)
          | Error _ -> error "Word error" )
      (*$((арифметическое выражение))*)
      | Subarifm ar ->
          let rezult_t = Inter_arifm.eval get_value_vari_int ar in
          let rezult =
            match rezult_t with
            | Ok number -> Some [string_of_int number]
            | Error _ -> None in
          return rezult
      (*$(...)*)
      | SubFn ex ->
          let new_global = Hashtbl.create 12345 in
          let new_local = Hashtbl.create 12345 in
          let _ = merdge_tables hash_table_global new_global in
          let _ = merdge_tables hash_table_local new_global in
          eval_expr None fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all new_global new_local ex in
    helper_eval_arg input_arifm

  (*>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<*)
  and eval_expr (from_pipe : string list option) fun_get_value_str
      fun_get_value_int fun_get_value_vari_int functions_all hash_table_global
      hash_table_local input_expr =
    let get_value_str = fun_get_value_str hash_table_global hash_table_local in
    let get_value_int = fun_get_value_int hash_table_global hash_table_local in
    let get_value_of_vari_int =
      fun_get_value_vari_int hash_table_global hash_table_local in
    let helper_eval_exp from_pipe_in input_expr =
      match input_expr with
      (*a=b, список для массива array = (hello world), в ином случае список из 1 элемента*)
      (*  vari * arg list Equal (...,[]) - присваивание ничего в переменную или в массив*)
      | Eqal (perem, val_lst) -> (
        try
          let name =
            match perem with
            | SimpleVari str -> str
            | LocalVari (SimpleVari str) -> str
            | ArrayDecl str -> str
            | LocalVari (ArrayDecl str) -> str
            | _ -> failwith "Command not found" in
          let arg_help elem =
            type_out_elem
              (eval_arg fun_get_value_str fun_get_value_int
                 fun_get_value_vari_int functions_all hash_table_global
                 hash_table_local elem) in
          let arg_lst =
            List.concat
              (List.filter_map Fun.id
                 (List.map (fun elem -> arg_help elem) val_lst)) in
          let val_lst =
            List.map
              (fun elem ->
                if check_str elem then Value (VInt, elem)
                else Value (VString, elem))
              arg_lst in
          let first_value =
            match val_lst with head :: _ -> head | _ -> Value (VString, "")
          in
          let lokaliti = match perem with LocalVari _ -> true | _ -> false in
          let sig_var =
            match perem with
            | LocalVari (SimpleVari _) -> true
            | SimpleVari _ -> true
            | _ -> false in
          if sig_var then
            let varii =
              {sig_value= first_value; is_array= false; array_value= []} in
            if lokaliti then
              let _ = Hashtbl.replace hash_table_local name varii in
              return None
            else
              let _ = Hashtbl.replace hash_table_global name varii in
              return None
          else
            let varii =
              {sig_value= first_value; is_array= true; array_value= val_lst}
            in
            if lokaliti then
              let _ = Hashtbl.replace hash_table_local name varii in
              return None
            else
              let _ = Hashtbl.replace hash_table_global name varii in
              return None
        with
        | Failure str -> error str
        | _ -> error "error in eqal expression" )
      (* string * arg list * redirect option имя функции, список аргументов, редирект ввода вывода(редиректа нет - он содержит пустые строки)*)
      | CallFunction (name, ar_lst, redir_opt) -> (
        try
          let custom_fun_solver =
            fun_solver fun_get_value_str fun_get_value_int
              fun_get_value_vari_int functions_all in
          let arg_help elem =
            type_out_elem
              (eval_arg fun_get_value_str fun_get_value_int
                 fun_get_value_vari_int functions_all hash_table_global
                 hash_table_local elem) in
          let arg_lst =
            List.concat
              (List.filter_map Fun.id
                 (List.map (fun elem -> arg_help elem) ar_lst)) in
          let cont_lst = string_to_var_contain_lst arg_lst in
          let is_redir = match redir_opt with None -> false | Some _ -> true in
          let is_redir_in =
            if is_redir then
              match redir_opt with
              | Some (Redirect (_, Some _, _)) -> true
              | _ -> false
            else false in
          let is_redir_out =
            if is_redir then
              match redir_opt with
              | Some (Redirect (Some _, _, _)) -> true
              | _ -> false
            else false in
          let is_from_pipe =
            match from_pipe_in with Some _ -> true | None -> false in
          let from_pipe_cont =
            string_to_var_contain_lst
              (match from_pipe_in with Some x -> x | None -> [""]) in
          let pair_lst =
            list_to_pair
              ( if is_from_pipe then from_pipe_cont
              else if is_redir_in then
                let filename =
                  match redir_opt with
                  | Some (Redirect (_, Some file, _)) -> file
                  | _ -> "" in
                string_to_var_contain_lst (str_lst_from_file filename)
                (*from file pure ???????*)
              else cont_lst ) in
          let new_local = Hashtbl.create 12345 in
          let _ = merdge_tables_list pair_lst new_local in
          match redir_opt with
          | Some (Redirect (Some file, _, _)) -> (
            try
              let res_lst =
                match custom_fun_solver name hash_table_global new_local with
                | Some x -> x
                | None -> [""] in
              let _ = str_lst_to_file file res_lst in
              return None
            with _ -> error "error in function/in file out" (*pure*) )
          | _ -> (
            try return (custom_fun_solver name hash_table_global new_local)
            with _ -> error "error in function" )
        with exn -> (
          match exn with Failure str -> error str | _ -> error "bad command" ) )
    in
    helper_eval_exp from_pipe input_expr

  (*может в исключения*)
  (*добавляем возможные функции*)
  and fun_solver fun_get_value_str fun_get_value_int fun_get_value_vari_int
      functions_all name hash_global hash_local =
    let standart_functions = ["echo"] in
    if List.mem name standart_functions then
      match name with
      | "echo" -> StandartLib.echo_function hash_global hash_local
      | _ -> None
    else if Hashtbl.mem functions_all name then
      let conveyr = (Hashtbl.find functions_all name).comands in
      try
        type_out_elem
          (eval_pipe_conv fun_get_value_str fun_get_value_int
             fun_get_value_vari_int functions_all hash_global hash_local
             conveyr)
      with _ -> failwith "bad commands in fun_solver"
    else StandartLib.run_function name hash_global hash_local

  (*pure*)

  (*>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<*)
  and eval_pipe from_pipe fun_get_value_str fun_get_value_int
      fun_get_value_vari_int functions_all hash_table_global hash_table_local
      input_pipe =
    let module Inter_arifm = Interpreter_Arifm (Result) in
    let get_value_str = fun_get_value_str hash_table_global hash_table_local in
    let get_value_int = fun_get_value_int hash_table_global hash_table_local in
    let get_value_of_vari_int =
      fun_get_value_vari_int hash_table_global hash_table_local in
    let helper_pipe from_pipe_local input_pipe =
      match input_pipe with
      | Expression ex ->
          eval_expr from_pipe_local fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all hash_table_global
            hash_table_local ex
      (*while (()) do done arifm * pipeConveyor*)
      | While (ar_cond, conv) -> (
          let rec loop () =
            let cond =
              match Inter_arifm.eval get_value_of_vari_int ar_cond with
              | Ok x -> x
              | Error str -> failwith str in
            if cond != 0 then
              let value =
                eval_pipe_conv fun_get_value_str fun_get_value_int
                  fun_get_value_vari_int functions_all hash_table_global
                  hash_table_local conv in
              value
              >>= fun elem ->
              loop ()
              >>= fun tail ->
              match elem with
              | None -> (
                match tail with
                | None -> return None
                | Some second_lst -> return (Some second_lst) )
              | Some first_lst -> (
                match tail with
                | None -> return (Some first_lst)
                | Some second_lst -> return (Some (first_lst @ second_lst)) )
            else return None in
          try loop ()
          with e -> (
            match e with Failure str -> error str | _ -> error "DEAD LOOP" ) )
      (*arifm * pipeConveyor * pipeConveyor option if (()) then else*)
      | IfElse (conditial, first_conv, sec_opt) -> (
          let cond =
            match Inter_arifm.eval get_value_of_vari_int conditial with
            | Ok x -> x
            | Error str -> failwith str in
          if cond != 0 then
            eval_pipe_conv fun_get_value_str fun_get_value_int
              fun_get_value_vari_int functions_all hash_table_global
              hash_table_local first_conv
          else
            match sec_opt with
            | None -> return None
            | Some sec_conv ->
                eval_pipe_conv fun_get_value_str fun_get_value_int
                  fun_get_value_vari_int functions_all hash_table_global
                  hash_table_local sec_conv )
      (*string * arg list * pipeConveyor ||| variable, list of values, todoList*)
      | Foreach (var, arg_lst, pip_conv) -> (
          let arg_help elem =
            type_out_elem
              (eval_arg fun_get_value_str fun_get_value_int
                 fun_get_value_vari_int functions_all hash_table_global
                 hash_table_local elem) in
          let arg_lst_str =
            List.concat
              (List.map
                 (function Some lst -> lst | None -> [""])
                 (List.filter
                    (fun elem ->
                      match elem with Some _ -> true | None -> false)
                    (List.map (fun elem -> arg_help elem) arg_lst))) in
          let arg_lst_cont = string_to_var_contain_lst arg_lst_str in
          let temp_lst =
            List.map
              (fun elem ->
                let _ = Hashtbl.replace hash_table_global var elem in
                eval_pipe_conv fun_get_value_str fun_get_value_int
                  fun_get_value_vari_int functions_all hash_table_global
                  hash_table_local pip_conv)
              arg_lst_cont in
          try
            let result =
              List.concat (List.filter_map Fun.id (type_out_lst temp_lst)) in
            return (Some result)
          with e -> (
            match e with Failure str -> error str | _ -> error "bad for" ) )
      (*WHILE INTERPRETER*) in
    helper_pipe from_pipe input_pipe

  and eval_pipe_conv fun_get_value_str fun_get_value_int fun_get_value_vari_int
      functions_all hash_table_global hash_table_local input_pipe_conv =
    let rec helper_pipe_conv (from_pipe_lock : string list option)
        input_pip_conv =
      match input_pip_conv with
      | SigPipe pip ->
          eval_pipe from_pipe_lock fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all hash_table_global
            hash_table_local pip
      (*pipeOp * pipe * pipeConveyor*)
      | Pipline (Dpoint, pip, pip_conv) -> (
          let first_rezult =
            eval_pipe from_pipe_lock fun_get_value_str fun_get_value_int
              fun_get_value_vari_int functions_all hash_table_global
              hash_table_local pip in
          try
            return (type_out_elem first_rezult)
            >>= fun first ->
            helper_pipe_conv None pip_conv
            >>= fun second ->
            match first with
            | Some fir -> (
              match second with
              | Some sec -> return (Some (fir @ sec))
              | None -> return (Some fir) )
            | None -> (
              match second with
              | Some sec -> return (Some sec)
              | None -> return None )
          with Failure _ ->
            helper_pipe_conv None pip_conv >>= fun second -> return second )
      | Pipline (Redi, pip, pip_conv) ->
          let right_res = helper_pipe_conv from_pipe_lock pip_conv in
          let right_res_stlst = try type_out_elem right_res with _ -> None in
          eval_pipe right_res_stlst fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all hash_table_global
            hash_table_local pip
          >>= fun left -> return left
      | Pipline (And, pip, pip_conv) -> (
          let first_rezult =
            eval_pipe from_pipe_lock fun_get_value_str fun_get_value_int
              fun_get_value_vari_int functions_all hash_table_global
              hash_table_local pip in
          first_rezult
          >>= fun first ->
          helper_pipe_conv None pip_conv
          >>= fun second ->
          match first with
          | Some fir -> (
            match second with
            | Some sec -> return (Some (fir @ sec))
            | None -> return (Some fir) )
          | None -> (
            match second with
            | Some sec -> return (Some sec)
            | None -> return None ) )
      | Pipline (Or, pip, pip_conv) -> (
          let first_rezult =
            eval_pipe from_pipe_lock fun_get_value_str fun_get_value_int
              fun_get_value_vari_int functions_all hash_table_global
              hash_table_local pip in
          try return (type_out_elem first_rezult)
          with _ ->
            helper_pipe_conv None pip_conv >>= fun second -> return second )
    in
    helper_pipe_conv None input_pipe_conv

  and eval_decl_func functions_all funct =
    match funct with
    | Method (str, conv) ->
        let my_func = {comands= conv} in
        let _ = Hashtbl.replace functions_all str my_func in
        "" |> ignore

  and eval_bCommand fun_get_value_str fun_get_value_int fun_get_value_vari_int
      functions_all hash_table_global
      (hash_table_local : (string, variable_container) Hashtbl.t) cmd =
    match cmd with
    | DeclFunct decl ->
        let _ = eval_decl_func functions_all decl in
        return None
    | PipeConv conveyr ->
        eval_pipe_conv fun_get_value_str fun_get_value_int
          fun_get_value_vari_int functions_all hash_table_global
          hash_table_local conveyr

  and eval_bCmdConv fun_get_value_str fun_get_value_int fun_get_value_vari_int
      functions_all hash_table_global hash_table_local bcmd_conv =
    let rec helper_bCmdConv bcmd =
      match bcmd with
      | BSigCmd cmd ->
          eval_bCommand fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all hash_table_global
            hash_table_local cmd
      | BConv (cmd, conv) -> (
          eval_bCommand fun_get_value_str fun_get_value_int
            fun_get_value_vari_int functions_all hash_table_global
            hash_table_local cmd
          >>= fun first ->
          helper_bCmdConv conv
          >>= fun second ->
          match first with
          | Some fir -> (
            match second with
            | Some sec -> return (Some (fir @ sec))
            | None -> return (Some fir) )
          | None -> (
            match second with
            | Some sec -> return (Some sec)
            | None -> return None ) ) in
    helper_bCmdConv bcmd_conv

  let eval_all functions_all variable_global variable_local input =
    try
      let result =
        type_out_elem
          (eval_bCmdConv get_value_of_container_string
             get_value_of_container_int get_value_of_vari_int functions_all
             variable_global variable_local input) in
      match result with Some lst_str -> lst_str | None -> [""]
    with Failure str -> ["FAil script: " ^ str]
end
