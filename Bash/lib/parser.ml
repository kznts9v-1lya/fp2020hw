open Opal
open Ast2
open Format

(* эта функция применяет парсер к строке *)
let apply p s = parse p (LazyStream.of_string s)

(* тут производится парсинг целых чисел и операция + - * / *)
let digits = spaces >> many1 digit => implode
let integer = digits => int_of_string
let temp_integer c = Int (int_of_string c)

(*промежуточный перевод*)

let some_integer = digits => temp_integer

(*возвращает int  в нужном формате и остается парсером*)

let floater =
  digits
  >>= fun fir ->
  token "." >> digits >>= fun sec -> return (float_of_string (fir ^ "." ^ sec))

let some_floater =
  digits
  >>= fun fir ->
  token "." >> digits
  >>= fun sec -> return (Float (float_of_string (fir ^ "." ^ sec)))

let reserved =
  [ "if"; "else"; "fi"; "for"; "in"; "while"; "do"; "done"; "end"; "local"
  ; "return" ]

(*returns string which is not in reserved*)
let bash_ident =
  many1 alpha_num => implode
  >>= function s when List.mem s reserved -> mzero | s -> return s

let some_bash_ident =
  many1 (alpha_num <|> one_of ['.'; '/'; '-'; '_'])
  => implode
  >>= function s when List.mem s reserved -> mzero | s -> return (String s)

(*had to be like ident but точки тоже включает в слово*)
let ident_p =
  spaces
  >> (letter <~> many (alpha_num <|> one_of ['.']))
  => implode
  >>= function s when List.mem s reserved -> mzero | s -> return s

(*had to be like ident, but with points.*)

let some_ident =
  many (alpha_num <|> one_of ['.'])
  => implode
  >>= function s when List.mem s reserved -> mzero | s -> return (String s)

let parens = between (token "(") (token ")")
let add_op = token "+" >> return (fun x y -> Plus (x, y))
let sub_op = token "-" >> return (fun x y -> Minus (x, y))
let multi_op = token "*" >> return (fun x y -> Multi (x, y))
let div_op = token "/" >> return (fun x y -> Divide (x, y))
let and_op = token "&&" >> return (fun x y -> AndAr (x, y))
let or_op = token "||" >> return (fun x y -> OrAr (x, y))
let great_op = token ">" >> return (fun x y -> GreatAr (x, y))
let less_op = token "<" >> return (fun x y -> LessAr (x, y))
let ecGreat_op = token ">=" >> return (fun x y -> EcGreatAr (x, y))
let ecLess_op = token "<=" >> return (fun x y -> EcLessAr (x, y))
let ecualy_op = token "==" >> return (fun x y -> EcualyAr (x, y))

(*--------------------------------------------------------------------------------------------------------------------*)
(*Парсим const vari arifm*)

let rec pars_vari input =
  (pars_bracesEx <|> pars_array <|> pars_Simplevari) input

and pars_bracesEx input =
  ( token "$"
  >> between (token "{") (token "}") pars_variNot
  >>= fun var -> return (Braces var) )
    input

and pars_Simplevari input =
  (token "$" >> bash_ident >>= fun str -> return (SimpleVari str)) input

and pars_array input =
  ( bash_ident
  >>= fun str ->
  between (token "[") (token "]") arifm
  >>= fun index -> return (ArrayVari (str, index)) )
    input

and pars_variNot input = (pars_array <|> pars_SimplevariNot) input

and pars_SimplevariNot input =
  (bash_ident >>= fun str -> return (SimpleVari str)) input

(*----------*)
and vari_to_container input =
  (pars_vari >>= fun var -> return (Variable var)) input

and pars_container input =
  (vari_to_container <|> some_floater <|> some_integer <|> some_ident) input

and pars_container_word input =
  (vari_to_container <|> some_floater <|> some_integer <|> some_bash_ident)
    input

and pars_container_arifm input =
  (pars_container >>= fun ar -> return (Container ar)) input

and arifm input =
  chainl1 arifmetic
    ( and_op <|> or_op <|> ecGreat_op <|> ecLess_op <|> great_op <|> less_op
    <|> ecualy_op )
    input

and arifmetic input = chainl1 term (add_op <|> sub_op) input
and term input = chainl1 factor (multi_op <|> div_op) input
and factor input = (parens arifm <|> pars_container_arifm) input

let local_vari_eqal input =
  ( token "local" >> spaces >> bash_ident
  >>= fun str -> return (LocalVari (SimpleVari str)) )
    input

let vari_eqal input =
  (*используем в парсере expr, для приравнивания*)
  ( local_vari_eqal
  <|> (bash_ident >>= fun str -> return (SimpleVari str))
  <|> pars_array )
    input

(*
print_string "//////CONST+VARI TEST////////////\n"

let (str : string) = "${aa[1+${bb[2]}]}"


print_string (str ^ " = ")

print_string (containerOption_to_string (apply pars_container str))

let arifmOption_to_string d =
  match d with None -> "None" | Some x -> arifm_to_string x


print_string "\n//////ARIFM TEST////////////\n"

let (str : string) = "(3+5)==4.1 +${Path[1]}"


print_string (str ^ " = ")

print_string (arifmOption_to_string (apply arifm str))
*)
(*-----------------------Парсим WORD--------------------------------------------------------*)
let rec pars_word input =
  (wbrExpCont <|> wStringCont <|> wString <|> wbrExp) input

and wString input =
  (pars_container_word >>= fun con -> return (WString con)) input

and wbrExp input =
  (token "{" >> inBraceExp >>= fun brac -> token "}" >> return (WbrExp brac))
    input

and wStringCont input =
  ( pars_container_word
  >>= fun con ->
  pars_word_NoContainer >>= fun wor -> return (WStringCont (con, wor)) )
    input

and wbrExpCont input =
  ( token "{" >> inBraceExp
  >>= fun barc ->
  token "}" >> pars_word_NoBr >>= fun wor -> return (WbrExpCont (barc, wor)) )
    input

and pars_word_NoContainer input = (wbrExpCont <|> wbrExp) input
and pars_word_NoBr input = (wStringCont <|> wString) input

and inBraceExp input =
  ( many1 (pars_word >>= fun elem -> token "," >> return elem)
  >>= (fun lst -> pars_word >>= fun last -> return (InBracExpan (lst @ [last])))
  <|> (pars_word >>= fun only -> return (InBracExpan [only])) )
    input

(*
let ff = print_string "\n//////WORD TEST////////////\n"
let (str : string) = "ssss{${sds[1]},sss,}dsdsd"


print_string (str ^ " = ")

let ddddd = print_string (option_word_string (apply pars_word str))
*)
(*-----------------------------Парсим stroperator----------------------------------------------------------------------------*)

let rec pars_stroperator input =
  ( replaceEnd <|> replaceBeg <|> replaceAll <|> replaceFirst <|> cutEndMore
  <|> leng <|> cutBegMore <|> cutEndLess <|> cutBegLess <|> picFromPosLeng
  <|> picFromPos )
    input

and replaceEnd input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "/" >> token "%" >> pars_container
  >>= fun patrn ->
  token "/" >> pars_container
  >>= fun rep -> token "}" >> return (ReplaceEnd (str, patrn, rep)) )
    input

and replaceBeg input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "/" >> token "#" >> pars_container
  >>= fun patrn ->
  token "/" >> pars_container
  >>= fun rep -> token "}" >> return (ReplaceBeg (str, patrn, rep)) )
    input

and replaceAll input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "/" >> token "/" >> pars_container
  >>= fun patrn ->
  token "/" >> pars_container
  >>= fun rep -> token "}" >> return (ReplaceAll (str, patrn, rep)) )
    input

and replaceFirst input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "/" >> pars_container
  >>= fun patrn ->
  token "/" >> pars_container
  >>= fun rep -> token "}" >> return (ReplaceFirst (str, patrn, rep)) )
    input

and cutEndMore input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "%" >> token "%" >> pars_container
  >>= fun substr -> token "}" >> return (CutEndMore (str, substr)) )
    input

and cutBegMore input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "#" >> token "#" >> pars_container
  >>= fun substr -> token "}" >> return (CutBegMore (str, substr)) )
    input

and cutEndLess input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "%" >> pars_container
  >>= fun substr -> token "}" >> return (CutEndLess (str, substr)) )
    input

and cutBegLess input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token "#" >> pars_container
  >>= fun substr -> token "}" >> return (CutBegLess (str, substr)) )
    input

and picFromPosLeng input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token ":" >> pars_container
  >>= fun pos ->
  token ":" >> pars_container
  >>= fun leng -> token "}" >> return (PicFromPosLeng (str, pos, leng)) )
    input

and picFromPos input =
  ( token "$" >> token "{" >> pars_container
  >>= fun str ->
  token ":" >> pars_container
  >>= fun pos -> token "}" >> return (PicFromPos (str, pos)) )
    input

and leng input =
  ( token "$" >> token "{#" >> pars_container
  >>= fun str -> token "}" >> return (Leng str) )
    input

(*---------------------------------Парсим expr-------------------------------------------------------------------------*)
let rec pars_arg input = (sub_arifm <|> sub_fn <|> string_op <|> word_p) input

and sub_arifm input =
  (token "$((" >> arifm >>= fun ar -> token "))" >> return (Subarifm ar)) input

and sub_fn input =
  (token "$(" >> pars_expr >>= fun fn -> token ")" >> return (SubFn fn)) input

and string_op input =
  (pars_stroperator >>= fun strop -> return (StringOp strop)) input

and word_p input = (pars_word >>= fun wd -> return (Word wd)) input

and pars_expr input =
  (*<<<<<<<<<<<<<<<<*)
  (eqal_e <|> call_e) input

and eqal_e input = (array_decl_eq <|> single_eqal) input

and single_eqal input =
  ( vari_eqal
  >>= fun variable ->
  token "=" >> pars_arg >>= fun ag -> return (Eqal (variable, [ag])) )
    input

and array_decl input =
  ( token "local" >> spaces >> bash_ident
  >>= (fun arr -> return (LocalVari (ArrayDecl arr)))
  <|> (bash_ident >>= fun str -> return (ArrayDecl str)) )
    input

and array_decl_eq input =
  ( array_decl
  >>= fun massiv ->
  token "=(" >> pars_list_arg_array
  >>= fun args -> token ")" >> return (Eqal (massiv, args)) )
    input

and pars_list_arg_array input =
  ( pars_arg
  >>= fun beg ->
  many1 (space >> spaces >> pars_arg) >>= fun lst -> return (beg :: lst) )
    input

and call_e input =
  ( bash_ident
  >>= fun nam ->
  many (space >> pars_arg)
  (*pars_list_arg*)
  >>= fun arglst ->
  with_redirF nam arglst <|> with_redirL nam arglst
  <|> return (CallFunction (nam, arglst, None)) )
    input

and with_redirF nam arg input =
  ( token ">"
  >> (* > *)
  ident_p
  >>= fun red ->
  match arg with
  | [] ->
      return (CallFunction (nam, [], Some (Redirect (Some red, None, None))))
  | _ :: _ ->
      return (CallFunction (nam, arg, Some (Redirect (Some red, None, None))))
  )
    input

and with_redirL nam arg input =
  ( token "<"
  >> (* < *)
  ident_p
  >>= fun red ->
  match arg with
  | [] ->
      return (CallFunction (nam, [], Some (Redirect (None, Some red, None))))
  | _ :: _ ->
      return (CallFunction (nam, arg, Some (Redirect (None, Some red, None))))
  )
    input

(*
let ff = print_string "\n//////EXPR+ARG TEST////////////\n"

(*cat .w123.txt > txt.txt*)
let (str : string) = "ss=(111 33 ww $(cat .w123.txt))"


print_string (str ^ " <-> ")

let ddddd = print_string (option_expr_string (apply pars_expr str))
*)
(*-------------------------------------Парсим PIPE--------------------------------------------------------------------*)

let rec pars_pipeConv input =
  (parse_pipeline <|> single_pipe_conveyr >>= fun par -> return par) input

and single_pipe input =
  ( while_pars <|> ifelse_pars <|> foreach_pars <|> expr_pipe
  >>= fun par -> return par )
    input

and single_pipe_conveyr input =
  ( while_pars <|> ifelse_pars <|> foreach_pars <|> expr_pipe
  >>= fun par -> return (SigPipe par) )
    input

and parse_pipeline input =
  (p_dpoint <|> p_and <|> p_or <|> p_pipe >>= fun par -> return par) input

and p_dpoint input =
  ( single_pipe
  >>= fun fir ->
  spaces >> token ";" >> spaces >> pars_pipeConv
  >>= fun sec -> return (Pipline (Dpoint, fir, sec)) )
    input

and p_pipe input =
  ( single_pipe
  >>= fun fir ->
  spaces >> token "|" >> spaces >> pars_pipeConv
  >>= fun sec -> return (Pipline (Redi, fir, sec)) )
    input

and p_and input =
  ( single_pipe
  >>= fun fir ->
  spaces >> token "&&" >> spaces >> pars_pipeConv
  >>= fun sec -> return (Pipline (And, fir, sec)) )
    input

and p_or input =
  ( single_pipe
  >>= fun fir ->
  spaces >> token "||" >> spaces >> pars_pipeConv
  >>= fun sec -> return (Pipline (Redi, fir, sec)) )
    input

and expr_pipe input = (pars_expr >>= fun ar -> return (Expression ar)) input

and while_pars input =
  ( token "while"
  >> (* while *)
  spaces >> token "((" >> spaces >> arifm
  >>= fun pred ->
  spaces >> token "))" >> spaces >> token "do" >> spaces
  >> (* do *)
  pars_pipeConv
  >>= fun act -> spaces >> token "done" >> return (While (pred, act)) )
    input

and ifelse_pars input =
  ( token "if"
  >> (* if *)
  spaces >> token "((" >> spaces >> arifm
  >>= fun pred ->
  spaces >> token "))" >> spaces >> token "then" >> spaces
  >> (* then *)
  pars_pipeConv
  >>= fun thn ->
  spaces >> token "else"
  >> (* else *)
  spaces >> pars_pipeConv
  >>= fun els -> spaces >> token "fi" >> return (IfElse (pred, thn, Some els))
  )
    input

and foreach_pars input =
  ( token "for"
  >> (* foreach *)
  spaces >> bash_ident
  >>= fun variable ->
  spaces >> token "in"
  >> (*variable*)
  spaces >> token "(" >> pars_list_arg_array (* in array*)
  >>= fun args ->
  token ")" >> spaces >> token "do" >> spaces >> pars_pipeConv
  >>= fun action ->
  spaces >> token "done"
  >> (* end *)
  return (Foreach (variable, args, action)) )
    input

(*
let ff = print_string "\n//////PIPECONV TEST////////////\n"

let (str : string) =
  "while (($1 > 0)) do facto=$(($facto*$1)) ; 1=$(($1-1)) done"


print_string (str ^ "\n <-> \n")

let pp_str = "test () {pwd file.txt > my.txt; ss= 10+2}"
let ddddd = print_string (option_pipeConveyr_string (apply pars_pipeConv str))
*)

(*-------------------------------------Парсим DECLFUNTION----------------------------------------------------------*)
let pars_declFunction input =
  ( bash_ident
  >>= fun nam ->
  spaces >> token "(" >> token ")" >> spaces >> token "{" >> spaces
  >> pars_pipeConv
  >>= fun act -> spaces >> token "}" >> return (Method (nam, act)) )
    input

(*
let ff = print_string "\n//////DeclFUNCTION TEST////////////\n"

(*funcFactor () { facto=1 ; while [$1 > 0] do facto=$facto*$1 ; 1=$1-1 done }*)
let (str : string) = "myfunction (){ echo www.txt ; cat w123.txt | echo yra }"


print_string (str ^ "\n <-> \n")

let ddddd =
  print_string (option_declFunction_string (apply pars_declFunction str))
*)
(*------------------------------Парсим BCOMMAND и BCMDCONV--------------------------------------------------------*)

let rec pars_bComdConv input = (b_conv <|> bCmd_sig_cmd) input
and b_sig_cmd input = (b_func <|> b_pipeConv) input
and bCmd_sig_cmd input = (b_sig_cmd >>= fun bcmd -> return (BSigCmd bcmd)) input

and b_func input =
  (spaces >> pars_declFunction >>= fun fn -> return (DeclFunct fn)) input

and b_pipeConv input =
  (spaces >> pars_pipeConv >>= fun conv -> return (PipeConv conv)) input

and b_conv input =
  ( b_sig_cmd
  >>= fun bcmd ->
  space >> pars_bComdConv >>= fun conv -> return (BConv (bcmd, conv)) )
    input

(*---------------Back in AST-----------------------------------------------------*)
(*---------------Back in AST-----------------------------------------------------*)

let rec vari_to_string var =
  match var with
  | SimpleVari x -> sprintf "SimpleVari(\"%s\")" x
  | ArrayVari (name, index) ->
      sprintf "ArrayVari(\"%s\", %s)" name (arifm_to_string index)
  | ArrayDecl name -> sprintf "ArrayDecl(\"%s\")" name
  | Braces v -> sprintf "Braces(%s)" (vari_to_string v)
  | LocalVari v -> sprintf "LocalVari(%s)" (vari_to_string v)

and container_to_string ct =
  match ct with
  | Int c -> sprintf "Int(%d)" c
  | String d -> sprintf "String(\"%s\")" d
  | Float d -> sprintf "Float(%f)" d
  | Variable v -> sprintf "Variable(%s)" (vari_to_string v)

and arifm_to_string ex =
  match ex with
  | Plus (l, r) ->
      sprintf "Plus(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  | Minus (l, r) ->
      sprintf "Minus(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  | Multi (l, r) ->
      sprintf "Multi(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  | Divide (l, r) ->
      sprintf "Divide(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  | Container ct -> sprintf "Container(%s)" (container_to_string ct)
  (*a && b *)
  | AndAr (l, r) ->
      sprintf "AndAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a || b *)
  | OrAr (l, r) ->
      sprintf "OrAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a > b *)
  | GreatAr (l, r) ->
      sprintf "GreatAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a < b *)
  | LessAr (l, r) ->
      sprintf "LessAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a >= b *)
  | EcGreatAr (l, r) ->
      sprintf "EcGreatAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a =< b *)
  | EcLessAr (l, r) ->
      sprintf "EcLessAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)
  (*a == b *)
  | EcualyAr (l, r) ->
      sprintf "EcualyAr(%s , %s)" (arifm_to_string l) (arifm_to_string r)

let containerOption_to_string d =
  match d with None -> "None" | Some x -> container_to_string x

let rec word_string wor =
  match wor with
  | WString c -> sprintf "WString(%s)" (container_to_string c)
  | WbrExp brac -> sprintf "WbrExp(%s)" (brexp_to_string brac)
  | WStringCont (con, brac) ->
      sprintf "WStringCont(%s , %s)" (container_to_string con)
        (word_string brac)
  | WbrExpCont (brac, con) ->
      sprintf "WbrExpCont(%s , %s)" (brexp_to_string brac) (word_string con)

and brexp_to_string bra =
  let rec wordlst_to_string = function
    | [] -> "[]"
    | e :: l -> sprintf "%s::%s" (word_string e) (wordlst_to_string l) in
  match bra with
  | InBracExpan ls -> (
    match ls with
    | [elem] -> sprintf "[%s]" (word_string elem)
    | _ -> wordlst_to_string ls )

let option_word_string arg =
  match arg with None -> "None" | Some x -> word_string x

let option_string opt =
  match opt with None -> "None" | Some str -> sprintf "Some(%s)" str

let strOp_to_string stOp =
  match stOp with
  (*${#string}*)
  | Leng cont -> sprintf "Leng(%s)" (container_to_string cont)
  (*$ {string: position} *)
  | PicFromPos (cont_f, cont_s) ->
      sprintf "PicFromPos(%s , %s)"
        (container_to_string cont_f)
        (container_to_string cont_s)
  (*${string:position:length}*)
  | PicFromPosLeng (c_f, c_s, c_fi) ->
      sprintf "PicFromPos(%s, %s, %s)" (container_to_string c_f)
        (container_to_string c_s) (container_to_string c_fi)
  (*${string#substring}*)
  | CutBegLess (c_f, c_s) ->
      sprintf "CutBegLess(%s , %s)" (container_to_string c_f)
        (container_to_string c_s)
  (*${string%substring}*)
  | CutEndLess (c_f, c_s) ->
      sprintf "CutEndLess(%s , %s)" (container_to_string c_f)
        (container_to_string c_s)
  (*${string##substring}*)
  | CutBegMore (c_f, c_s) ->
      sprintf "CutBegMore(%s , %s)" (container_to_string c_f)
        (container_to_string c_s)
  (*${string%%substring}*)
  | CutEndMore (c_f, c_s) ->
      sprintf "CutEndMore(%s , %s)" (container_to_string c_f)
        (container_to_string c_s)
  (*${string/pattern/replacement}*)
  | ReplaceFirst (c_f, c_s, c_fi) ->
      sprintf "ReplaceFirst(%s , %s , %s)" (container_to_string c_f)
        (container_to_string c_s) (container_to_string c_fi)
  (*${string//pattern/replacement}*)
  | ReplaceAll (c_f, c_s, c_fi) ->
      sprintf "ReplaceAll(%s , %s , %s)" (container_to_string c_f)
        (container_to_string c_s) (container_to_string c_fi)
  (*${string/#pattern/replacement}*)
  | ReplaceBeg (c_f, c_s, c_fi) ->
      sprintf "ReplaceBeg(%s , %s , %s)" (container_to_string c_f)
        (container_to_string c_s) (container_to_string c_fi)
  (*${string/%pattern/replacement}*)
  | ReplaceEnd (c_f, c_s, c_fi) ->
      sprintf "ReplaceEnd(%s , %s , %s)" (container_to_string c_f)
        (container_to_string c_s) (container_to_string c_fi)

let rec arg_to_string ar =
  match ar with
  (*$ 2 + 2 *)
  | Subarifm x -> sprintf "Subarifm(%s)" (arifm_to_string x)
  (*$(...)*)
  | SubFn ex -> sprintf "SubFn(%s)" (expr_to_string ex)
  (*${#string}*)
  | StringOp st -> sprintf "StringOp(%s)" (strOp_to_string st)
  | Word wd -> sprintf "Word(%s)" (word_string wd)

and expr_to_string ex =
  match ex with
  (* a=b*)
  | Eqal (var, ar) ->
      sprintf "Eqal(%s , %s)" (vari_to_string var) (arglst_to_string ar)
  | CallFunction (str, arglst, Some (Redirect (fir, sec, thir))) ->
      sprintf "CallFunction(\"%s\" , %s , Some(Redirect(%s , %s , %s)) )" str
        (arglst_to_string arglst) (option_string fir) (option_string sec)
        (option_string thir)
      (*name, parametrs*)
  | CallFunction (str, arglst, None) ->
      sprintf "CallFunction(\"%s\" , %s , None )" str (arglst_to_string arglst)

and arglst_to_string = function
  | [] -> "[]"
  | a :: lis -> sprintf "%s::%s" (arg_to_string a) (arglst_to_string lis)

let option_expr_string = function None -> "NONE" | Some x -> expr_to_string x

let rec pipe_to_string pip =
  match pip with
  | Expression ar -> sprintf "Expression(%s)" (expr_to_string ar)
  | While (pred, p) ->
      sprintf "While(%s , %s)" (arifm_to_string pred) (pipeConveyor_to_string p)
  | IfElse (ar, l, Some r) ->
      sprintf "IfElse(%s , %s , Some(%s) )" (arifm_to_string ar)
        (pipeConveyor_to_string l) (pipeConveyor_to_string r)
  | IfElse (ar, l, None) ->
      sprintf "IfElse(%s , %s , None )" (arifm_to_string ar)
        (pipeConveyor_to_string l)
  | Foreach (var, arlst, act) ->
      sprintf "Foreach(\"%s\" , %s , %s)" var (arglst_to_string arlst)
        (pipeConveyor_to_string act)

and pipeConveyor_to_string conv =
  match conv with
  | SigPipe p -> sprintf "SigPipe(%s)" (pipe_to_string p)
  | Pipline (Dpoint, l, r) ->
      sprintf "Pipline(Dpoint, %s , %s)" (pipe_to_string l)
        (pipeConveyor_to_string r)
  | Pipline (Redi, l, r) ->
      sprintf "Pipeline(Redi, %s , %s)" (pipe_to_string l)
        (pipeConveyor_to_string r)
  | Pipline (And, l, r) ->
      sprintf "Pipeline(And, %s , %s)" (pipe_to_string l)
        (pipeConveyor_to_string r)
  | Pipline (Or, l, r) ->
      sprintf "Pipeline(Or, %s , %s)" (pipe_to_string l)
        (pipeConveyor_to_string r)

let declFunction_to_string dec =
  match dec with
  | Method (name, act) ->
      sprintf "Method(\"%s\" , %s)" name (pipeConveyor_to_string act)

let option_declFunction_string = function
  | None -> "NONE"
  | Some x -> declFunction_to_string x

let option_pipeConveyr_string pip =
  match pip with None -> "None!" | Some p -> pipeConveyor_to_string p

let bCommand_to_string = function
  | PipeConv x -> sprintf "PipeConv(%s)" (pipeConveyor_to_string x)
  | DeclFunct x -> sprintf "DeclFunct(%s)" (declFunction_to_string x)

let rec bCmdConv_to_string bcmd =
  match bcmd with
  | BSigCmd x -> sprintf "BSigCmd(%s)" (bCommand_to_string x)
  | BConv (x, y) ->
      sprintf "BConv(%s , %s)" (bCommand_to_string x) (bCmdConv_to_string y)

let option_bCmdConv_string = function
  | None -> "NONE"
  | Some x -> bCmdConv_to_string x

(*---------------Back in AST-----------------------------------------------------*)
(*---------------Back in AST END-----------------------------------------------------*)

(*Итоговая функция парсера*)
let pars_mini_bash input = apply pars_bComdConv input

(*
let ff = print_string "\n//////BCmd+BCommand TEST////////////\n"

let (str : string) =
  "funcFactor ()\n\
   {\n\
   facto=1 ;\n\
   count=$1 ;\n\
   while (( $count > 0 ))\n\
   do\n\
   facto=$(($facto*$count)) ; count=$(($count-1))\n\
   done ; echo $facto\n\
   }\n\n\
   funcFactor 4"


print_string (str ^ "\n <-> \n")

let ddddd = print_string (option_bCmdConv_string (apply pars_bComdConv str))
*)

(*--------------------------------------------------------------------------------------------------*)
let first = CallFunction ("echo", [Word (WString (String "eeee"))], None)
let second = SigPipe (Expression first)
let third = Method ("myfn", second)

let pred =
  BSigCmd
    (PipeConv
       (SigPipe
          (Expression
             (CallFunction ("echo", [Word (WString (String "rrr"))], None)))))

let result = BConv (DeclFunct third, pred)

let%test _ = apply pars_bComdConv "myfn (){echo eeee} echo rrr" = Some result
