open Ast2
open Format

let one_space = "    "

let rec decl_vari_to_string var =
  match var with
  | SimpleVari x -> x
  | ArrayVari (name, index) -> sprintf "%s[ %s]" name (arifm_to_string index)
  | ArrayDecl name -> name
  | LocalVari v -> sprintf "local %s" (decl_vari_to_string v)
  | _ -> ""

and vari_to_string var =
  match var with
  | SimpleVari x -> sprintf "$%s" x
  | ArrayVari (name, index) -> sprintf "%s[ %s]" name (arifm_to_string index)
  | ArrayDecl name -> name
  | Braces v -> sprintf "${ %s}" (vari_to_string v)
  | LocalVari v -> sprintf "local %s" (vari_to_string v)

and container_to_string ct =
  match ct with
  | Int c -> string_of_int c
  | String d -> d
  | Float d -> string_of_float d
  | Variable v -> vari_to_string v

and arifm_to_string ex =
  match ex with
  | Plus (l, r) -> sprintf "%s + %s" (arifm_to_string l) (arifm_to_string r)
  | Minus (l, r) -> sprintf "%s - %s" (arifm_to_string l) (arifm_to_string r)
  | Multi (l, r) -> sprintf "%s * %s" (arifm_to_string l) (arifm_to_string r)
  | Divide (l, r) -> sprintf "%s / %s" (arifm_to_string l) (arifm_to_string r)
  | Container ct -> container_to_string ct
  (*a && b *)
  | AndAr (l, r) -> sprintf "%s && %s" (arifm_to_string l) (arifm_to_string r)
  (*a || b *)
  | OrAr (l, r) -> sprintf "%s || %s" (arifm_to_string l) (arifm_to_string r)
  (*a > b *)
  | GreatAr (l, r) -> sprintf "%s > %s" (arifm_to_string l) (arifm_to_string r)
  (*a < b *)
  | LessAr (l, r) -> sprintf "%s < %s" (arifm_to_string l) (arifm_to_string r)
  (*a >= b *)
  | EcGreatAr (l, r) ->
      sprintf "%s >= %s" (arifm_to_string l) (arifm_to_string r)
  (*a <= b *)
  | EcLessAr (l, r) ->
      sprintf "%s <= %s" (arifm_to_string l) (arifm_to_string r)
  (*a == b *)
  | EcualyAr (l, r) ->
      sprintf "%s == %s" (arifm_to_string l) (arifm_to_string r)

let rec word_string wor =
  match wor with
  | WString c -> container_to_string c
  | WbrExp brac -> sprintf "{%s}" (brexp_to_string brac)
  | WStringCont (con, brac) ->
      sprintf "%s%s" (container_to_string con) (word_string brac)
  | WbrExpCont (brac, con) ->
      sprintf "{%s}%s" (brexp_to_string brac) (word_string con)

and brexp_to_string bra =
  let rec wordlst_to_string = function
    | head :: medium :: tail ->
        sprintf "%s %s" (word_string head) (wordlst_to_string (medium :: tail))
    | [head] -> word_string head
    | [] -> "" in
  match bra with InBracExpan ls -> wordlst_to_string ls

(*!!!!!!!!!!!*)
let option_string opt =
  match opt with None -> "None" | Some str -> sprintf "Some(%s)" str

let strOp_to_string stOp =
  match stOp with
  (*${#string}*)
  | Leng cont -> sprintf "${#%s}" (container_to_string cont)
  (*$ {string: position} *)
  | PicFromPos (cont_f, cont_s) ->
      sprintf "${%s:%s}"
        (container_to_string cont_f)
        (container_to_string cont_s)
  (*${string:position:length}*)
  | PicFromPosLeng (c_f, c_s, c_fi) ->
      sprintf "${%s:%s:%s}" (container_to_string c_f) (container_to_string c_s)
        (container_to_string c_fi)
  (*${string#substring}*)
  | CutBegLess (c_f, c_s) ->
      sprintf "${%s#%s}" (container_to_string c_f) (container_to_string c_s)
  (*${string%substring}*)
  | CutEndLess (c_f, c_s) ->
      sprintf "${%s%c%s}" (container_to_string c_f) '%'
        (container_to_string c_s)
  (*${string##substring}*)
  | CutBegMore (c_f, c_s) ->
      sprintf "${%s##%s}" (container_to_string c_f) (container_to_string c_s)
  (*${string%%substring}*)
  | CutEndMore (c_f, c_s) ->
      sprintf "${%s%s%s}" (container_to_string c_f) "%%"
        (container_to_string c_s)
  (*${string/pattern/replacement}*)
  | ReplaceFirst (c_f, c_s, c_fi) ->
      sprintf "${%s/%s/%s}" (container_to_string c_f) (container_to_string c_s)
        (container_to_string c_fi)
  (*${string//pattern/replacement}*)
  | ReplaceAll (c_f, c_s, c_fi) ->
      sprintf "${%s//%s/%s}" (container_to_string c_f) (container_to_string c_s)
        (container_to_string c_fi)
  (*${string/#pattern/replacement}*)
  | ReplaceBeg (c_f, c_s, c_fi) ->
      sprintf "${%s/#%s/%s}" (container_to_string c_f) (container_to_string c_s)
        (container_to_string c_fi)
  (*${string/%pattern/replacement}*)
  | ReplaceEnd (c_f, c_s, c_fi) ->
      sprintf "${%s/%c%s/%s}" (container_to_string c_f) '%'
        (container_to_string c_s) (container_to_string c_fi)

let rec arg_to_string ar =
  match ar with
  (*$ 2 + 2 *)
  | Subarifm x -> sprintf "$(( %s ))" (arifm_to_string x)
  (*$(...)*)
  | SubFn ex -> sprintf "$(%s)" (expr_to_string ex)
  (*${#string}*)
  | StringOp st -> strOp_to_string st
  | Word wd -> word_string wd

and expr_to_string ex =
  match ex with
  (* a=b*)
  | Eqal (var, ar) ->
      sprintf "%s=%s" (decl_vari_to_string var) (arglst_to_string ar)
  | CallFunction (str, arglst, Some (Redirect (Some fir, _, _))) ->
      sprintf "%s %s > %s " str (arglst_to_string arglst) fir
  | CallFunction (str, arglst, Some (Redirect (_, Some sec, _))) ->
      sprintf "%s %s < %s" str (arglst_to_string arglst) sec
  (*name, parametrs*)
  | CallFunction (str, arglst, None) ->
      sprintf "%s %s " str (arglst_to_string arglst)
  | _ -> ""

and arglst_to_string = function
  | head :: medium :: tail ->
      sprintf "%s %s" (arg_to_string head) (arglst_to_string (medium :: tail))
  | [head] -> arg_to_string head
  | [] -> ""

let rec pipe_to_string pip (actual_sp : string) =
  match pip with
  | Expression ar -> sprintf "%s%s" actual_sp (expr_to_string ar)
  | While (pred, p) ->
      sprintf "%swhile ((%s))\n%sdo \n%s%sdone" actual_sp (arifm_to_string pred)
        actual_sp
        (pipeConveyor_to_string p (actual_sp ^ one_space)
           (actual_sp ^ one_space))
        actual_sp
  | IfElse (ar, l, Some r) ->
      sprintf "%sif ((%s))\n%sthen\n%s%selse \n%s%sfi" actual_sp
        (arifm_to_string ar) actual_sp
        (pipeConveyor_to_string l (actual_sp ^ one_space)
           (actual_sp ^ one_space))
        actual_sp
        (pipeConveyor_to_string r (actual_sp ^ one_space)
           (actual_sp ^ one_space))
        actual_sp
  | IfElse (ar, l, None) ->
      sprintf "%sif ((%s)) \n%sthen\n%sfi" actual_sp (arifm_to_string ar)
        actual_sp
        (pipeConveyor_to_string l (actual_sp ^ one_space)
           (actual_sp ^ one_space))
  | Foreach (var, arlst, act) ->
      sprintf "%sfor %s in (%s) \n%sdo \n%s%sdone" actual_sp var
        (arglst_to_string arlst) actual_sp
        (pipeConveyor_to_string act (actual_sp ^ one_space)
           (actual_sp ^ one_space))
        actual_sp

and pipeConveyor_to_string conv (space : string) (actual_sp : string) =
  match conv with
  | SigPipe p -> sprintf "%s\n" (pipe_to_string p actual_sp)
  | Pipline (Dpoint, Expression l, r) ->
      sprintf "%s%s;\n%s" actual_sp
        (pipe_to_string (Expression l) "")
        (pipeConveyor_to_string r space space)
  | Pipline (Dpoint, l, r) ->
      sprintf "%s;\n%s"
        (pipe_to_string l actual_sp)
        (pipeConveyor_to_string r space space)
  | Pipline (Redi, l, r) ->
      sprintf "%s%s | %s" actual_sp (pipe_to_string l "")
        (pipeConveyor_to_string r space "")
  | Pipline (And, l, r) ->
      sprintf "%s%s && %s" actual_sp (pipe_to_string l "")
        (pipeConveyor_to_string r space "")
  | Pipline (Or, l, r) ->
      sprintf "%s%s || %s" actual_sp (pipe_to_string l "")
        (pipeConveyor_to_string r space "")

let declFunction_to_string dec =
  match dec with
  | Method (name, act) ->
      sprintf "%s () \n{\n%s}\n" name
        (pipeConveyor_to_string act one_space one_space)

let bCommand_to_string = function
  | PipeConv x -> sprintf "%s\n" (pipeConveyor_to_string x "" "")
  | DeclFunct x -> declFunction_to_string x

let rec pretty_printer_to_string bcmd =
  match bcmd with
  | BSigCmd x -> bCommand_to_string x
  | BConv (x, y) ->
      sprintf "%s%s" (bCommand_to_string x) (pretty_printer_to_string y)

let pretty_printer_option_string = function
  | None -> "NONE"
  | Some x -> pretty_printer_to_string x
