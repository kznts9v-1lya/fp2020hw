open Bash_lib.Ast2
open Bash_lib.Interpretator
open Bash_lib.Parser

let pars_to_interpretor script =
  let opt_result = pars_mini_bash script in
  match opt_result with Some x -> x | None -> failwith "NEVER DO THIS"

let rec print_list_string = function
  | [] -> ""
  | e :: l -> e ^ " " ^ print_list_string l

let arg = ReplaceFirst (String "12345567", String "123", String "www")

let mytest =
  let module E = Interpreter_StrOp (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  E.eval
    (get_value_of_container_string variable_global variable_local)
    (get_value_of_container_int variable_global variable_local)
    arg

let _ = print_string ("\n\n\nstrOp INTERPRITATE: " ^ mytest)

let wbexp =
  WbrExp
    (InBracExpan
       [WString (String "1"); WString (String "2"); WString (String "3")])

let wbeCon =
  WbrExpCont
    ( InBracExpan
        [WString (String "A"); WString (String "B"); WString (String "C")]
    , wbexp )

let arg = WStringCont (String "abc", wbeCon)

let mytest =
  let module E = Interpreter_Word (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  match
    E.eval (get_value_of_container_string variable_global variable_local) arg
  with
  | Ok x -> x
  | _ -> ["NOOOOOOOOOOOOOOOOOO"]

let _ =
  print_string ("\n\n\nword INTERPRITATE: " ^ print_list_string mytest ^ "\n")

(*---------------Factorial test-------------------------------------------------------*)
let script =
  "funcFactor ()\n\
   {\n\
   facto=1 ;\n\
   count=$1 ;\n\
   while (( $count > 0 ))\n\
   do\n\
   facto=$(($facto*$count)) ; count=$(($count-1))\n\
   done ; echo $facto\n\
   }\n\n\
   funcFactor 7"

let _ = print_string ("\n\n\nall INTERPRITATE:\n" ^ script ^ "\n>>>>>\n")
let result_arg = pars_to_interpretor script

let mytest =
  let module E = Interpreter_ArgExpr_bash_all (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  let functions_all = Hashtbl.create 1234 in
  E.eval_all functions_all variable_global variable_local result_arg

let _ = print_string ("result: " ^ print_list_string mytest ^ "\n")

(*--------------- test even odd-------------------------------------------------------*)
let script =
  "even () {\n\
   local temp=$1;\n\
  \      if (( $temp == 0 )) \n\n\
  \        then\n\
   echo true\n\
   else\n\n\
  \   if (( $temp < 0 )) \n\n\
  \          then\n\
  \        echo nil\n\
  \      else\n\
  \      echo $(odd $(($temp - 1)))\n\
  \      fi\n\n\
  \   fi\n\
   }\n\
   odd () {\n\
   local temp=$1;\n\
  \      if (( $temp == 1 )) \n\n\
  \         then\n\
   echo true\n\
   else\n\n\
  \   if (( $temp < 0 )) \n\n\
  \      then\n\
  \        echo nil\n\
  \      else\n\
  \      echo $(even $(($temp - 1)))\n\
  \      fi\n\n\
  \   fi\n\
   }\n\
   even 6\n"

let _ = print_string ("\n\n\nall INTERPRITATE:\n" ^ script ^ "\n>>>>>\n")
let result_arg = pars_to_interpretor script

let mytest =
  let module E = Interpreter_ArgExpr_bash_all (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  let functions_all = Hashtbl.create 1234 in
  E.eval_all functions_all variable_global variable_local result_arg

let _ = print_string ("result: " ^ print_list_string mytest ^ "\n")

(*---------------strOp test-------------------------------------------------------*)
let script = "a=1234 ; echo ${#} ${#$s} ${#$a}"
let _ = print_string ("\n\n\nall INTERPRITATE:\n" ^ script ^ "\n>>>>>\n")
let result_arg = pars_to_interpretor script

let mytest =
  let module E = Interpreter_ArgExpr_bash_all (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  let functions_all = Hashtbl.create 1234 in
  E.eval_all functions_all variable_global variable_local result_arg

let _ = print_string ("result: " ^ print_list_string mytest ^ "\n")

(*--------------- test out command-------------------------------------------------------*)
let script = "echo | find /home/user/Projects_OCaml/gitlab/fp2020hw -maxdepth 1"
let _ = print_string ("\n\n\nall INTERPRITATE:\n" ^ script ^ "\n>>>>>\n")
let result_arg = pars_to_interpretor script

let mytest =
  let module E = Interpreter_ArgExpr_bash_all (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  let functions_all = Hashtbl.create 1234 in
  E.eval_all functions_all variable_global variable_local result_arg

let _ = print_string ("result: " ^ print_list_string mytest ^ "\n")

(*--------------- test pipe &&-------------------------------------------------------*)
let script = "echo 1 && echo 2"
let _ = print_string ("\n\n\nall INTERPRITATE:\n" ^ script ^ "\n>>>>>\n")
let pars_result_f = option_bCmdConv_string (apply pars_bComdConv script)
let _ = print_string (pars_result_f ^ "\n")
let result_arg = pars_to_interpretor script

let mytest =
  let module E = Interpreter_ArgExpr_bash_all (Result) in
  let variable_global = Hashtbl.create 1234 in
  let variable_local = Hashtbl.create 1234 in
  let functions_all = Hashtbl.create 1234 in
  E.eval_all functions_all variable_global variable_local result_arg

let _ = print_string ("result: " ^ print_list_string mytest ^ "\n")
