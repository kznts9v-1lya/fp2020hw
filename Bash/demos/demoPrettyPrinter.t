  $ (cd ../../../../default && demos/demoPrettyPrinter.exe)
   funcFactor ()
  {
  facto=1 ;
  count=$1 ;
  while (( $count > 0 ))
  do
  facto=$(($facto*$count)) ; count=$(($count-1))
  done ; echo $facto
  }
  
  funcFactor 4
   <-> 
  BConv(DeclFunct(Method("funcFactor" , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Word(WString(Int(1)))::[])) , Pipline(Dpoint, Expression(Eqal(SimpleVari("count") , Word(WString(Variable(SimpleVari("1"))))::[])) , Pipline(Dpoint, While(GreatAr(Container(Variable(SimpleVari("count"))) , Container(Int(0))) , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Subarifm(Multi(Container(Variable(SimpleVari("facto"))) , Container(Variable(SimpleVari("count")))))::[])) , SigPipe(Expression(Eqal(SimpleVari("count") , Subarifm(Minus(Container(Variable(SimpleVari("count"))) , Container(Int(1))))::[]))))) , SigPipe(Expression(CallFunction("echo" , Word(WString(Variable(SimpleVari("facto"))))::[] , None )))))))) , BSigCmd(PipeConv(SigPipe(Expression(CallFunction("funcFactor" , Word(WString(Int(4)))::[] , None ))))))
  Pretty print:
  funcFactor () 
  {
      facto=1;
      count=$1;
      while (($count > 0))
      do 
          facto=$(( $facto * $count ));
          count=$(( $count - 1 ))
      done;
      echo $facto 
  }
  funcFactor 4 
  
  
   -----------------------
  BConv(DeclFunct(Method("funcFactor" , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Word(WString(Int(1)))::[])) , Pipline(Dpoint, Expression(Eqal(SimpleVari("count") , Word(WString(Variable(SimpleVari("1"))))::[])) , Pipline(Dpoint, While(GreatAr(Container(Variable(SimpleVari("count"))) , Container(Int(0))) , Pipline(Dpoint, Expression(Eqal(SimpleVari("facto") , Subarifm(Multi(Container(Variable(SimpleVari("facto"))) , Container(Variable(SimpleVari("count")))))::[])) , SigPipe(Expression(Eqal(SimpleVari("count") , Subarifm(Minus(Container(Variable(SimpleVari("count"))) , Container(Int(1))))::[]))))) , SigPipe(Expression(CallFunction("echo" , Word(WString(Variable(SimpleVari("facto"))))::[] , None )))))))) , BSigCmd(PipeConv(SigPipe(Expression(CallFunction("funcFactor" , Word(WString(Int(4)))::[] , None ))))))
  Pretty print:
  funcFactor () 
  {
      facto=1;
      count=$1;
      while (($count > 0))
      do 
          facto=$(( $facto * $count ));
          count=$(( $count - 1 ))
      done;
      echo $facto 
  }
  funcFactor 4 
  
  
  
  
  Compare: GOOD
  -------------------------
  even () {
  local temp=$1;
        if (( $temp == 0 )) 
  
          then
  echo true
  else
  
     if (( $temp < 0 )) 
  
            then
          echo nil
        else
        echo $(odd $(($temp - 1)))
        fi
  
     fi
  }
  odd () {
  local temp=$1;
        if (( $temp == 1 )) 
  
           then
  echo true
  else
  
     if (( $temp < 0 )) 
  
        then
          echo nil
        else
        echo $(odd $(($temp - 1)))
        fi
  
     fi
  }
  even 6
   <-> 
  BConv(DeclFunct(Method("even" , Pipline(Dpoint, Expression(Eqal(LocalVari(SimpleVari("temp")) , Word(WString(Variable(SimpleVari("1"))))::[])) , SigPipe(IfElse(EcualyAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("true")))::[] , None ))) , Some(SigPipe(IfElse(LessAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("nil")))::[] , None ))) , Some(SigPipe(Expression(CallFunction("echo" , SubFn(CallFunction("odd" , Subarifm(Minus(Container(Variable(SimpleVari("temp"))) , Container(Int(1))))::[] , None ))::[] , None )))) ))) ))))) , BConv(DeclFunct(Method("odd" , Pipline(Dpoint, Expression(Eqal(LocalVari(SimpleVari("temp")) , Word(WString(Variable(SimpleVari("1"))))::[])) , SigPipe(IfElse(EcualyAr(Container(Variable(SimpleVari("temp"))) , Container(Int(1))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("true")))::[] , None ))) , Some(SigPipe(IfElse(LessAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("nil")))::[] , None ))) , Some(SigPipe(Expression(CallFunction("echo" , SubFn(CallFunction("odd" , Subarifm(Minus(Container(Variable(SimpleVari("temp"))) , Container(Int(1))))::[] , None ))::[] , None )))) ))) ))))) , BSigCmd(PipeConv(SigPipe(Expression(CallFunction("even" , Word(WString(Int(6)))::[] , None )))))))
  Pretty print:
  even () 
  {
      local temp=$1;
      if (($temp == 0))
      then
          echo true 
      else 
          if (($temp < 0))
          then
              echo nil 
          else 
              echo $(odd $(( $temp - 1 )) ) 
          fi
      fi
  }
  odd () 
  {
      local temp=$1;
      if (($temp == 1))
      then
          echo true 
      else 
          if (($temp < 0))
          then
              echo nil 
          else 
              echo $(odd $(( $temp - 1 )) ) 
          fi
      fi
  }
  even 6 
  
  
   -----------------------
  BConv(DeclFunct(Method("even" , Pipline(Dpoint, Expression(Eqal(LocalVari(SimpleVari("temp")) , Word(WString(Variable(SimpleVari("1"))))::[])) , SigPipe(IfElse(EcualyAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("true")))::[] , None ))) , Some(SigPipe(IfElse(LessAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("nil")))::[] , None ))) , Some(SigPipe(Expression(CallFunction("echo" , SubFn(CallFunction("odd" , Subarifm(Minus(Container(Variable(SimpleVari("temp"))) , Container(Int(1))))::[] , None ))::[] , None )))) ))) ))))) , BConv(DeclFunct(Method("odd" , Pipline(Dpoint, Expression(Eqal(LocalVari(SimpleVari("temp")) , Word(WString(Variable(SimpleVari("1"))))::[])) , SigPipe(IfElse(EcualyAr(Container(Variable(SimpleVari("temp"))) , Container(Int(1))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("true")))::[] , None ))) , Some(SigPipe(IfElse(LessAr(Container(Variable(SimpleVari("temp"))) , Container(Int(0))) , SigPipe(Expression(CallFunction("echo" , Word(WString(String("nil")))::[] , None ))) , Some(SigPipe(Expression(CallFunction("echo" , SubFn(CallFunction("odd" , Subarifm(Minus(Container(Variable(SimpleVari("temp"))) , Container(Int(1))))::[] , None ))::[] , None )))) ))) ))))) , BSigCmd(PipeConv(SigPipe(Expression(CallFunction("even" , Word(WString(Int(6)))::[] , None )))))))
  Pretty print:
  even () 
  {
      local temp=$1;
      if (($temp == 0))
      then
          echo true 
      else 
          if (($temp < 0))
          then
              echo nil 
          else 
              echo $(odd $(( $temp - 1 )) ) 
          fi
      fi
  }
  odd () 
  {
      local temp=$1;
      if (($temp == 1))
      then
          echo true 
      else 
          if (($temp < 0))
          then
              echo nil 
          else 
              echo $(odd $(( $temp - 1 )) ) 
          fi
      fi
  }
  even 6 
  
  
  
  
  Compare: GOOD
  -------------------------
