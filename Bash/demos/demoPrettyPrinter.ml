open Bash_lib.Pretty_printer
open Bash_lib.Parser

let str_to_file filename data =
  let oc = open_out filename in
  (* create or truncate file, return channel *)
  Printf.fprintf oc "%s" data;
  (* write something *)
  close_out oc

(*---------------Test Block begin---------------------------------------*)
let (str_bash : string) =
  " funcFactor ()\n\
   {\n\
   facto=1 ;\n\
   count=$1 ;\n\
   while (( $count > 0 ))\n\
   do\n\
   facto=$(($facto*$count)) ; count=$(($count-1))\n\
   done ; echo $facto\n\
   }\n\n\
   funcFactor 4"

let start_print = print_string (str_bash ^ "\n <-> \n")
let pars_result_f = option_bCmdConv_string (apply pars_bComdConv str_bash)
let _ = print_string pars_result_f

let pretty_result_f =
  pretty_printer_option_string (apply pars_bComdConv str_bash)

let _ =
  print_string
    ("\nPretty print:\n" ^ pretty_result_f ^ "\n -----------------------\n")

let pars_result_s =
  option_bCmdConv_string (apply pars_bComdConv pretty_result_f)

let _ = print_string pars_result_s

let pretty_result_s =
  pretty_printer_option_string (apply pars_bComdConv pretty_result_f)

let _ = print_string ("\nPretty print:\n" ^ pretty_result_s ^ "\n")

let _ =
  if compare pretty_result_s pretty_result_f = 0 then
    print_string "\n\nCompare: GOOD\n-------------------------\n"
  else print_string "\n\nCompare: BAD\n-------------------------\n"

(*---------------Test Block end---------------------------------------*)
(*---------------Test Block begin---------------------------------------*)
let (str_bash : string) =
  "even () {\n\
   local temp=$1;\n\
  \      if (( $temp == 0 )) \n\n\
  \        then\n\
   echo true\n\
   else\n\n\
  \   if (( $temp < 0 )) \n\n\
  \          then\n\
  \        echo nil\n\
  \      else\n\
  \      echo $(odd $(($temp - 1)))\n\
  \      fi\n\n\
  \   fi\n\
   }\n\
   odd () {\n\
   local temp=$1;\n\
  \      if (( $temp == 1 )) \n\n\
  \         then\n\
   echo true\n\
   else\n\n\
  \   if (( $temp < 0 )) \n\n\
  \      then\n\
  \        echo nil\n\
  \      else\n\
  \      echo $(odd $(($temp - 1)))\n\
  \      fi\n\n\
  \   fi\n\
   }\n\
   even 6"

let start_print = print_string (str_bash ^ "\n <-> \n")
let pars_result_f = option_bCmdConv_string (apply pars_bComdConv str_bash)
let _ = print_string pars_result_f

let pretty_result_f =
  pretty_printer_option_string (apply pars_bComdConv str_bash)

let _ =
  print_string
    ("\nPretty print:\n" ^ pretty_result_f ^ "\n -----------------------\n")

let pars_result_s =
  option_bCmdConv_string (apply pars_bComdConv pretty_result_f)

let _ = print_string pars_result_s

let pretty_result_s =
  pretty_printer_option_string (apply pars_bComdConv pretty_result_f)

let _ = print_string ("\nPretty print:\n" ^ pretty_result_s ^ "\n")

let _ =
  if pretty_result_s = pretty_result_f then
    print_string "\n\nCompare: GOOD\n-------------------------\n"
  else print_string "\n\nCompare: BAD\n-------------------------\n"

(*---------------Test Block end---------------------------------------*)
