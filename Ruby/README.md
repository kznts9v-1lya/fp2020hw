### An implementaion of Ruby mini-language

This is a homework for functional programming course.

License: LGPL

Author: Ivanov Kirill, ivanov.kirill.a@mail.ru

Features done:

- 1 ast;
- 2 parser;
- 3 pretty-printer;
- 4 interpreter;
- 5 stdlib + repl;
- 6 tests.

Features in progress:

- 1 search for bugs.

Замечания:

- поддерживается следующий синтаксис присваиваний: v0, v1, ... , vn = expr0, ... ;
- x = y = z = expression -- не поддерживается;
- методы без круглых скобок не поддерживаются;
- поддерживаются строки в двойных кавычках;
- лямбды не поддерживают замыкания;
- вложенные классы и методы не поддерживаются;
- списки - это питоновские списки, то есть динамические массивы;
- программа исполняется в контексте экземпляра main класса Object (согласно ruby);
- ключевое слово 'self' при объявлении метода внутри класса эквивалентно объявлению метода класса;
- метод без ассоциированной с ним переменной (def method ...) является методом экземпляра класса.